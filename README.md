## Language Code Formatter

Language Code Formatter provides a way to display the internal
language code of an entity by extending the Language field formatter.
Normally, the Language field formatter outputs the pretty name of the
language, but sometimes you need the code.

Example: I want the language code inside of a view in order to create
relative paths in another view text field with the correct langcode.

### Install

 * Install as you would normally install a contributed Drupal module.
 * Edit something that uses the Language field type and select this
"Language Code" formatter.


### Maintainers
George Anderson (geoanders)
https://www.drupal.org/u/geoanders
