<?php

namespace Drupal\language_code_formatter\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FieldItemInterface;
use Drupal\Core\Field\Plugin\Field\FieldFormatter\LanguageFormatter;
use Drupal\Core\Form\FormStateInterface;

/**
 * Plugin implementation of the 'language_code' formatter.
 *
 * @FieldFormatter(
 *   id = "language_code",
 *   label = @Translation("Language Code"),
 *   field_types = {
 *     "language"
 *   }
 * )
 */
class LanguageCodeFormatter extends LanguageFormatter {

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $form = parent::settingsForm($form, $form_state);
    $form['native_language']['#access'] = FALSE;
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  protected function viewValue(FieldItemInterface $item) {
    return [
      '#plain_text' => $item->language ?? $item->language->getId(),
    ];
  }

}
